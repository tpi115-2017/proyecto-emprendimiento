//Variable global para control.
var counter = 1;
//Agrega una nueva fila al formulario.
function addInput(divName){
        //Se crea un nuevo div. Se hace de esta forma para que el nuevo elemento no se ponga debajo del boton submit
          var newdiv = document.createElement('div');
          //Se agrega el nuevo elemento al div creado Se encierra en un parrafo para facilitar el borrado.
          newdiv.innerHTML = "<p id='parrafoAlumno"+counter+"'> Estudiante Nuevo/a: <input name='Nombre' type='text' placeholder='Nombres' required /> <input name='Apellido' type='text' placeholder='Apellidos' required /> <button type='button' onClick='deleteInput("+(counter)+");'>-</button></p> ";
          //Se agrega el div en el template. Segun el id del parametro divName.
          document.getElementById(divName).appendChild(newdiv);
          counter++; //Se genera un contador para generar un id unico por elemento. (El parrafo donde se inserta)
          //agregar "required" a los nuevos elementos no basta para validar el form.
          //Hay que generar a pie la regla. Al eliminar el elemento tambien se elimina la regla.
          $(newdiv).each(function() {
                $(this).rules("add", 
                    {
                        required: true
                    })
            }); 
}
//Borra un elemento previamente agragado al form.
function deleteInput(numero){
//Solo recibe el numero del parrafo que se desea eliminar. Facilita el borrado.
var elemento = document.getElementById('parrafoAlumno'+numero)
//Nos vamos al elemento padre (parentNode) y borramos el parrafo con todo su contenido.
elemento.parentNode.removeChild(elemento);
}



