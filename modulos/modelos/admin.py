# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from modulos.modelos.models import Departamento, Municipio, Escuela, Alumno, Grado, Inscripcion, Materia, Evaluacion, Aviso, Docente, Periodo, NotaEvaluacion, NotaGlobal, PersonalAdministrativo
# Register your models here.


admin.site.register(Departamento)
admin.site.register(Municipio)
admin.site.register(Escuela)
admin.site.register(Aviso)
admin.site.register(Docente)
admin.site.register(Alumno)
admin.site.register(Grado)
admin.site.register(Inscripcion)
admin.site.register(Periodo)
admin.site.register(Materia)
admin.site.register(Evaluacion)
admin.site.register(NotaEvaluacion)
admin.site.register(NotaGlobal)
admin.site.register(PersonalAdministrativo)