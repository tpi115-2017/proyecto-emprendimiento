# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django import forms
# Importando el User de Django para establecer relaciones entre modelos Alumno y Docente
from django.contrib.auth.models import User
## Import de los modelos Usuario y Rol por si hace falta usarlos
# from modulos.usuarios.models import Usuario, Rol

# Create your models here.
class Departamento(models.Model):
	def __unicode__(self):
		return self.nombre
	nombre = models.CharField(max_length=50)	

class Municipio(models.Model):
	id_departemento = models.ForeignKey(Departamento, on_delete=models.CASCADE, null=True)
	nombre = models.CharField(max_length=100)
	def __unicode__(self):
		return self.nombre

class Escuela(models.Model):
	nombre = models.CharField(max_length=255)
	id_municipio = models.ForeignKey(Municipio, null=True)
	direccion = models.TextField(max_length=512)
	def __unicode__(self):
		return self.nombre


class Materia(models.Model):
	id_escuela = models.ForeignKey(Escuela, null=True)
	nombreMateria = models.CharField(max_length=100)
	def __unicode__(self):
		return self.nombreMateria

class PersonalAdministrativo(models.Model):
	id_escuela = models.ForeignKey(Escuela)
	id_usuario = models.OneToOneField(User, blank=True, null=True, on_delete=models.CASCADE)


# Cambios: Quitados los campos activo y apellidos, se conserva nombres porque es el valor de retorno de __unicode__
# Se manejan los tipos de usuarios como si fueran proxy models de django, esto implica que el modelo docente este
# relacionado con un usuario unicamente. F. Kendal Sosa
class Docente(models.Model):
	id_escuela = models.ForeignKey(Escuela)
	id_usuario = models.OneToOneField(User, blank=True, null=True, on_delete=models.CASCADE)
	materias = models.ManyToManyField(Materia)
	nombres = models.CharField(max_length=100)
	telefono = models.CharField(max_length=9, null=True)
	correo_docente = models.EmailField(max_length=100, null=True)
	direccion = models.TextField(max_length=255, null=False)
	def __unicode__(self):
		return self.nombres

class Grado(models.Model):
	id_escuela = models.ForeignKey(Escuela, null=True)
	id_encargado = models.ForeignKey(Docente, null=True)
	nombreGrado = models.CharField(max_length=50)
	numeroGrado = models.PositiveSmallIntegerField(null=True)
	activo = models.BooleanField()

	def __unicode__(self):
		
		return self.nombreGrado
		

class AlumnoManager(models.Manager):
	def create_alumno(self, nombres, id_escuela, id_usuario):
		alumno = self.create(nombres= nombres, id_escuela = id_escuela, id_usuario= id_usuario)
		return alumno

# Cambios: Quitados los campos de activo y apellidos, se deja el campo nombre porque la funcion getNombres utiliza
# este atributo, lo demas campos se puede quedar con el user de django.
# Se maneja el modelo alumno como si fuera un modelo proxy de django. un Alumno tiene relacion con un unico 
# usuario de django. F. Kendal Sosa
class Alumno(models.Model):
	id_escuela = models.ForeignKey(Escuela)
	id_usuario = models.OneToOneField(User, blank=True, null=True, on_delete=models.CASCADE)
	nombres = models.CharField(max_length=100)
	sexo = models.CharField(max_length=10, null=True)
	fechaNacimiento = models.DateField(auto_now=False, auto_now_add=False, null=True)
	direccion = models.TextField(max_length=255, null=True)
	nombreResponsable = models.CharField(max_length=255, null=True)
	numeroResponsable = models.CharField(max_length=10, null=True)

	objects = AlumnoManager()

	def __unicode__(self):
		return self.nombres

	def getNombres(self):
		return self.nombres

class Aviso(models.Model):
	autor = models.ForeignKey(User, null=True, blank=True)
	destinatario = models.ForeignKey(Alumno, null=True)
	fechaPublicacion = models.DateTimeField(auto_now=True, auto_now_add=False)
	encabezado = models.CharField(max_length=140)
	mensaje = models.CharField(max_length=500)
	tipo = models.CharField(max_length=50)
	def __unicode__(self):
		return self.encabezado

class Inscripcion(models.Model):
	id_grado = models.ForeignKey(Grado)
	id_alumno = models.ForeignKey(Alumno)
	anio = models.PositiveSmallIntegerField()
	fechaHoraInscripcion = models.DateTimeField(auto_now=False, auto_now_add=False)		
	activo = models.BooleanField()

	#agregue al metodo unicode que devuelva el nombre del alumno y su grado
	#como una sola cadena pero no sé que les parece si quieren lo cambian
	def __unicode__(self):
		return self.id_alumno.nombres + ' en ' + self.id_grado.nombreGrado

class Evaluacion(models.Model):
	id_materia = models.ForeignKey(Materia)
	id_grado = models.ForeignKey(Grado)
	nombre = models.CharField(max_length=128)
	ponderacion = models.DecimalField(max_digits=7, decimal_places=4)
	def __unicode__(self):
		return self.nombre

class Periodo(models.Model):
	id_evaluacion = models.ForeignKey(Evaluacion)
	id_grado = models.ForeignKey(Grado)
	
class NotaEvaluacion(models.Model):
	id_evaluacion = models.ForeignKey(Evaluacion)
	id_alumno = models.ForeignKey(Alumno)
	nota = models.DecimalField(max_digits=4, decimal_places=2)

	def __unicode__(self):
		return 'nota del alumno: ' + self.id_alumno.nombres

class NotaGlobal(models.Model):
	id_alumno = models.ForeignKey(Alumno, null=True, blank=True)
	id_materia = models.ForeignKey(Materia, null=True, blank=True)
	notaTotal = models.DecimalField(max_digits=4, decimal_places=2, null=True, blank=True)



