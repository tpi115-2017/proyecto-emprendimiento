from django import forms
from django.forms import ModelForm


from modulos.modelos.models import Departamento, Municipio, Evaluacion, Aviso, Escuela, Materia, Inscripcion, NotaEvaluacion, Alumno
from modulos.modelos.models import Grado, Departamento, Municipio, Evaluacion, Aviso, Escuela, Materia, Inscripcion, Alumno, Periodo

class DepartamentoForm(forms.ModelForm):
	class Meta:
		model = Departamento
		fields = [
			'nombre',

		]
		labels = {
			'nombre' : 'Nombre de Departamento',
		}

		widgets = {
			'nombre': forms.TextInput(attrs={'class ': 'form-control'}),
		}
class MunicipioForm(forms.ModelForm):
	class Meta:
		model = Municipio
		fields = [
			'id_departemento',
			'nombre',
		]
		labels = {
			'id_departemento': 'Municipio',
			'nombre' : 'Nombre de Municipio',
		}

		widgets = {
			'id_departemento': forms.Select(attrs={'class':'form-control'}),
			'nombre': forms.TextInput(attrs={'class ': 'form-control'}),
		}
class EvaluacionForm(forms.ModelForm):
	class Meta:
		model = Evaluacion
		fields = [
			'id_materia',
			'id_grado',
			'nombre',
			'ponderacion',

		]
		labels = {
			'id_materia'	: 'Nombre de la materia',
			'id_grado' 		: 'Numero de grado',
			'nombre'  		: 'Nombre de evaluacion',
			'ponderacion'	: 'Ponderacion',
		}

		widgets = {
			'id_materia'	: forms.Select(attrs={'class':'form-control', 'required': True}),
			'id_grado'		: forms.Select(attrs={'class':'form-control', 'required': True}),
			'nombre'		: forms.TextInput(attrs={'class':'form-control', 'required': True}),
			'ponderacion'	: forms.NumberInput(attrs={'class': 'form-control', 'required': True}),
			
		}



class AvisoForm(forms.ModelForm):
	class Meta:
		model = Aviso
		fields = [
			'encabezado',
			'mensaje',
			'tipo',
			'destinatario',

		]
		labels = {
			'encabezado'	: 'Titulo',
			'mensaje'		: 'Contenido',
			'tipo' 			: 'Caracter de Aviso',
			'destinatario'	: 'Destinatario',
		}

		widgets = {
			'encabezado'	: forms.TextInput(attrs={'class':'form-control', 'required': True}),
			'mensaje'		: forms.TextInput(attrs={'class':'form-control', 'required': True}),
			'tipo'			: forms.TextInput(attrs={'class':'form-control', 'required': True}),
			#Me falta hacer que destinatario reciba solo los alumnos del encargado si es docente
			#O lo dejamos asi? xD
			'destinatario'	: forms.Select(attrs={'class':'form-control', 'required': False}),
			
		}
	#Se me ocurre que si dejamos vacio destinatario, asumiriamos que el anuncio es para todos.
	#Agregue esta parte para que pueda guardarse como null. 
	def __init__(self, *args, **kwargs):
		super(AvisoForm, self).__init__(*args, **kwargs)
		self.fields['destinatario'].required = False

class EscuelaForm(forms.ModelForm):
	class Meta:
		model = Escuela
		fields = [
		'nombre',
		'direccion',
		'id_municipio',
		]
		labels = {
		'nombre'		: 'Nombre',
		'direccion'		: 'Direccion',
		'id_municipio' 	: 'Municipio',
		}
		widgets = {
		'nombre'		: forms.TextInput(attrs={'class':'form-control', 'required': True}),
		'direccion'		: forms.TextInput(attrs={'class':'form-control', 'required': True}),
		'id_municipio'  : forms.Select(attrs={'class':'form-control', 'required': True}),
		}

class MateriaForm(forms.ModelForm):
	class Meta:
		model = Materia
		fields = [
		'id_escuela',
		'nombreMateria',
		]
		labels = {
		'id_escuela'	: 'Escuela',
		'nombreMateria'	: 'Nombre de Asignatura',
		}
		widgets = {
		'id_escuela'	: forms.Select(attrs={'class':'form-control', 'required': True}),
		'nombreMateria'	: forms.TextInput(attrs={'class':'form-control', 'required': True}),
		}

class InscripcionForm(forms.ModelForm):
	class Meta:
		model = Inscripcion
		fields = [
			'id_grado',
			'id_alumno',
			'anio',
			'fechaHoraInscripcion',
			'activo',
		]
		labels = {
			'id_grado'             : 'Grado',
			'id_Alumno'            : 'Alumno',
			'anio'                 : 'Anio',
			'fechaHoraInscripcion' : 'Fecha y Hora de Inscripcion',
			'activo'               : 'Activo',
		}
		widgets = {
			'id_grado'             : forms.Select(attrs={'class':'form-control', 'required':True}),
			'id_alumno'            : forms.Select(attrs={'class':'form-control', 'required':True}),
			'anio'                 : forms.NumberInput(attrs={'class':'form-control', 'required':True}),
			'fechaHoraInscripcion' : forms.DateTimeInput(attrs={'class':'form-control', 'required':True, 'type': 'date'}),
			'activo'               : forms.CheckboxInput(attrs={'class':'form-control', 'required':False}),
		}


class NotaEvaluacionForm(forms.ModelForm):
	class Meta:
		model = NotaEvaluacion
		fields = [
			'id_alumno',
			'id_evaluacion',
			'nota',
		]
		labels = {
			'id_alumno'            : 'Alumno',
			'id_evaluacion'        : 'Evaluacion',
			'nota'                 : 'Nota',
			
		}
		widgets = {
			'id_alumno'           : forms.Select(attrs={'class':'form-control', 'required':True}),
			'id_evaluacion'        : forms.Select(attrs={'class':'form-control', 'required':True}),
			'nota'                 : forms.NumberInput(attrs={'class':'form-control', 'step':'0.01', 'required':True}),
			
		}
		
		

class AlumnoForm(forms.ModelForm):
	CHOICES= [('Masculino','Masculino'),('Femenino','Femenimo')]
	sexo = forms.ChoiceField(choices=CHOICES, widget=forms.RadioSelect())
	class Meta:
		model = Alumno
		fields = [
			'sexo',
			'fechaNacimiento',
			'direccion',
			'nombreResponsable',
			'numeroResponsable',
		]
		labels = {
			'sexo'		: 'Sexo',
			'fechaNacimiento'	: 'Fecha de Nacimiento',
			'direccion'			: 'Direccion',
			'nombreResponsable'	: 'Nombre del Responsable',
			'numeroResponsable'	: 'Telefono del Responsable',
		}
		widgets= {
			'fechaNacimiento'	: forms.DateTimeInput(attrs={'class':'form-control', 'required':False, 'type': 'date'}),
			'direccion'			: forms.TextInput(attrs={'class':'form-control', 'required': False}),
			'nombreResponsable'	: forms.TextInput(attrs={'class':'form-control', 'required': False}),
			'numeroResponsable' : forms.TextInput(attrs={'class':'form-control', 'required': False}),

		}


class GradoForm(forms.ModelForm):
	CHOICES= [(True ,'Activo'),(False ,'Inactivo')]
	activo= forms.ChoiceField(choices=CHOICES, widget=forms.RadioSelect())
	class Meta:
		model = Grado
		fields = [
			'nombreGrado',
			'numeroGrado',
			'activo',
			'id_encargado',
		]
		labels = {
			'nombreGrado'	: 'Nombre del Grado',
			'numeroGrado'	: 'Numero de Grado',
			'activo'		: 'Estado',
			'id_encargado'	: 'Docente Encargado',
		}
		widgets = {
			'nombreGrado'	: forms.TextInput(attrs={'class':'form-control', 'required': False}),
			'numeroGrado' 	: forms.NumberInput(attrs={'class':'form-control', 'required':True}),
			'id_encargado'	: forms.Select(attrs={'class':'form-control', 'required':False}),
		}
	def __init__(self, *args, **kwargs):
		super(GradoForm, self).__init__(*args, **kwargs)
		self.fields['id_encargado'].required = False

class PeriodoForm(forms.ModelForm):
	class Meta:
		model = Periodo
		fields = [
			'id_evaluacion',
			'id_grado',
		]
		labels = {
			'id_evaluacion'	: 'Evaluacion',
			'id_grado'		: 'Grado',
		}
		widgets = {
			'id_evaluacion' : forms.Select(attrs={'class':'form-control', 'required':True}),
			'id_grado'		: forms.Select(attrs={'class':'form-control', 'required':True}),
		}