from django.conf.urls import url, include

from modulos.vistas import views
from material.frontend import urls as frontend_urls
from django.contrib.auth.views import LogoutView  ## Vista de cierre de Sesion

# Estandarizacion de URLs (Aplicar una ves terminada la aplicacion)
# Patron para URLS: /siraips/<rol>/<crud>/<accion>/
# Ej: /siraips/docente/grados/consultar
# Revisado por: Kendal Sosa (@kendalalfonso37)

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^logout/$', LogoutView.as_view(next_page='/'), name='logout'),
    # url(r'^inicio$', views.inicio, name="inicio"), ## Estas urls asumo que tampoco funcionaran ya 
    # url(r'^cerrar_sesion$', views.logout, name="cerrar_sesion"),
    url(r'^admin/', include(frontend_urls)), # redirige solo a /admin/ pero al iniciar sesion lo hara desde /siraips/admin/
    
    # VISTAS PARA EL PERSONAL ADMINISTRATIVO
    #Municipio
    url(r'^municipio/crear', views.municipio_view, name='mun_crear'),

    #Crud de departamento
    url(r'^departamento/listar', views.departamento_list, name='departamento_listar'),
    url(r'^departamento/crear', views.departamento_view, name='departamento_crear'),
    url(r'^departamento/editar/(?P<id_dep>\d+)$', views.departamento_edit, name='departamento_edit'),
    url(r'^departamento/eliminar/(?P<id_dep>\d+)$', views.departamento_del, name='departamento_delete'),

    #Crud Avisos
    url(r'^avisos/listar$', views.listar_avisos, name='listar_avisos'),
    url(r'^avisos/crear$', views.crear_avisos, name='crear_avisos'),
    url(r'^avisos/editar/(?P<id_aviso>\d+)$', views.editar_avisos, name='editar_avisos'),
    url(r'^avisos/eliminar/(?P<id_aviso>\d+)$', views.eliminar_aviso, name='eliminar_aviso'),
    url(r'^avisos/alumno$', views.mostrar_avisos, name= 'avisos_estudiante'),

    #Crud Escuela
    url(r'^escuela/listar$', views.listar_escuela, name='listar_escuela'),
    url(r'^escuela/crear$', views.crear_escuela, name='crear_escuela'),
    url(r'^escuela/editar/(?P<id_escuela>\d+)$', views.editar_escuela, name='editar_escuela'),
    url(r'^escuela/eliminar/(?P<id_escuela>\d+)$', views.eliminar_escuela, name='eliminar_escuela'),

    #Crud Materias
    url(r'^materia/listar$', views.listar_materia, name='listar_materia'),
    url(r'^materia/crear$', views.crear_materia, name='crear_materia'),
    url(r'^materia/editar/(?P<id_materia>\d+)$', views.editar_materia, name='editar_materia'),
    url(r'^materia/eliminar/(?P<id_materia>\d+)$', views.eliminar_materia, name='eliminar_materia'),
    
    # Crud de Inscripciones (Autor: Kendal Sosa [kendalalfonso37])
    url(r'^inscripcion/listar$', views.listar_inscripciones, name='listar_inscripciones'),
    url(r'^inscripcion/crear$', views.crear_inscripcion, name='crear_inscripcion'),
    url(r'^inscripcion/editar/(?P<id_inscripcion>\d+)$', views.editar_inscripcion, name='editar_inscripcion'),
    url(r'^inscripcion/eliminar/(?P<id_inscripcion>\d+)$', views.eliminar_inscripcion, name='eliminar_inscripcion'),

    #crud de Gestion de Alumnos
    url(r'^gestion/alumno/consultar$', views.listar_alumno, name='listar_alumno'),
    url(r'^gestion/alumno/eliminar/(?P<user_id>\d+)$', views.eliminar_alumno, name='eliminar_alumno'),
    url(r'^gestion/alumno/editar/(?P<user_id>\d+)$', views.editar_alumno, name='editar_alumno'),
    url(r'^gestion/alumno/crear$', views.crear_alumno, name='crear_alumno'),

    #crud para gestion de Grados
    url(r'^gestion/grados/consultar$', views.listar_grado, name='listar_grado'),
    url(r'^gestion/grados/eliminar/(?P<id_grado>\d+)$', views.eliminar_grado, name='eliminar_grado'),
    url(r'^gestion/grados/editar/(?P<id_grado>\d+)$', views.editar_grado, name='editar_grado'),
    url(r'^gestion/grados/crear$', views.crear_grado, name='crear_grado'),


    # VISTAS DE URLS PARA DOCENTES:
    # url para ir consultar los grados
    url(r'^grados/consultar$', views.list_grades_per_level, name='grades'),

    #url para consultar los estudiantes por grado, se relaciona con la url grades
    url(r'^grados/consultar/estudiantes/(?P<id_grado>\d+)$', views.list_students, name='grades_al'),

    #url para consultar las materias que el profesor le imparte al alumno
    url(r'^grados/consultar/estudiantes/materias/(?P<id_al>\d+)$', views.list_subjects_per_student, name='sub_al'),

    # Crud de evaluacion
    url(r'^evaluacion/crear$', views.crear_evaluacion, name='evaluation'),
    url(r'^evaluacion/listar/(?P<id_sub>\d+)$', views.listar_evaluacion, name='list_evaluation'),
    url(r'^evaluacion/editar/(?P<id_evaluacion>\d+)$', views.editar_evaluacion, name='edit_evaluation'),
    url(r'^evaluacion/eliminar/(?P<id_evaluacion>\d+)$', views.eliminar_evaluacion, name='del_evaluation'),
    url(r'^evaluacion/elegir/materia$', views.list_subjects_evaluation, name='elegir_sub'),
    
    # CRUD de Periodos
    url(r'^periodo/listar$', views.listar_periodos, name='listar_periodos'),
    url(r'^periodo/crear$', views.crear_periodo, name='crear_periodo'),
    url(r'^periodo/editar/(?P<id_periodo>\d+)$', views.editar_periodo, name='editar_periodo'),
    url(r'^periodo/eliminar/(?P<id_periodo>\d+)$', views.eliminar_periodo, name='eliminar_periodo'),

    # VISTAS DE URLS PARA ALUMNOS:
    # Consultar nota para el alumno (supongo)

    #consultar notas para el profesor :V antes era para el alumno
    url(r'^grados/consultar/estudiantes/materias/evaluacion/(?P<id_subject>\d+)/notas/(?P<id_ev>\d+)$', views.list_evaluation, name='notas1'),
    url(r'^notas/evaluacion/create$', views.create_grades, name='notas_crear'),
    #URL PARA ALUMNOS
    url(r'^nota/consultar$', views.consultar_notas_alumno, name='alumno_nota'),


]
