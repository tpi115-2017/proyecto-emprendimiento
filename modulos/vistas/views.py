# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required # Decorador para login requerido
from modulos.modelos.models import Departamento, Municipio, Escuela, Alumno, Grado, Inscripcion, Materia, Evaluacion, Aviso, Docente, Periodo, NotaEvaluacion, NotaGlobal, PersonalAdministrativo
# from modulos.usuarios.models import Usuario, Rol
from django.http import HttpResponse
from django.db.models import Q


from modulos.modelos.forms import DepartamentoForm, MunicipioForm, EvaluacionForm, AvisoForm, EscuelaForm, MateriaForm, InscripcionForm, AlumnoForm, GradoForm, NotaEvaluacionForm, PeriodoForm

from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth.models import User
import datetime
from django.contrib.auth.models import Group


#=====Generacion de PDFs====
from io import BytesIO

from django.http import HttpResponse
from django.views.generic import ListView
from reportlab.platypus import SimpleDocTemplate, Paragraph, TableStyle
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.lib import colors
from reportlab.lib.pagesizes import letter
from reportlab.platypus import Table

# Create your views here.

@login_required #Por cada vista que requiera un inicio de sesion se utilizara este decorador
def index(request):
	'''Esta vista redirige al index.html, la validacion de la presentacion de acuerdo
	a los roles que tiene cara usuario se hace aqui mismo dependiendo del grupo al que pertenece el usuario.'''
	if request.user.groups.filter(name = "Personal Administrativo"):
		return render(request, "index/index_admin.html", {})
	elif request.user.groups.filter(name = "Alumnos"):
		return render(request, "index/index_alumno.html", {})
	elif request.user.groups.filter(name = "Docentes"):
		return render(request, "index/index_docente.html", {})
	else:
		return render(request, "index/index_denied.html", {})

# =============== CRUD Departamento ( Personal Administrativo ) ===============
# Objetivo: Crear Departamentos desde la interfaz del Personal Administrativo

def departamento_view(request):
	if request.method == 'POST':
		form = DepartamentoForm(request.POST)
		if form.is_valid():
			form.save()
		return redirect('dep_listar')
	else:
		form = DepartamentoForm()
	return render(request, "dep_crear.html", {'form':form})

def departamento_list(request):
	dep = Departamento.objects.all().order_by('id')
	return render(request, 'dep_listar.html', {'departamentos':dep})

def departamento_edit(request, id_dep):
	dep = Departamento.objects.get(id = id_dep)
	if request.method == 'GET':
		form = DepartamentoForm(instance = dep)
	else:
		form = DepartamentoForm(request.POST,instance = dep)
		if form.is_valid():
			form.save()
		return redirect('dep_listar')
	return render(request, 'dep_crear.html', {'form':form})

def departamento_del(request, id_dep):
	dep = Departamento.objects.get(id = id_dep)
	if request.method == 'POST':
		dep.delete()
		return redirect('dep_listar')
	return render(request, 'dep_delete.html', {'departamento': dep})

# =============== CRUD Municipio ( Personal Administrativo ) ===============
# Objetivo: Ingresar Municipios desde la interfaz del Personal Administrativo
def municipio_view(request):
	if request.method == 'POST':
		form = MunicipioForm(request.POST)
		if form.is_valid():
			form.save()
		return redirect(index)
	else:
		form = MunicipioForm()
	return render(request, 'mun_crear.html', {'form': form})

# =============== Consultar Notas (Alumno) ===============
# Objetivo: Retornar las notas del Alumno que consulta
def create_grades(request):
	if request.method == 'POST':
		form = NotaEvaluacionForm(request.POST)
		if form.is_valid():
			form.save()
		return redirect('index')
	else:
		form = NotaEvaluacionForm()
	return render(request, 'create_grades.html', {'form':form})

def consultar_notas_alumno(request):
	alumnmo = Alumno.objects.get(id_usuario = request.user.id)
	nota_global = NotaGlobal.objects.filter(id_alumno = alumnmo.id)
	nota_evaluaciones = NotaEvaluacion.objects.filter(id_alumno = alumnmo.id)
	return render(request, 'alumnos/notas_alumno.html', {'nota_g': nota_global, 'nota_e': nota_evaluaciones})

def list_evaluation(request, id_subject, id_ev):
	ev = Evaluacion.objects.filter(id_materia = id_ev).order_by('id')
	grades =  NotaEvaluacion.objects.filter(id_alumno = id_subject).order_by('id')
	try:
		notaT = NotaGlobal.objects.get(id_materia = id_ev, id_alumno = id_subject)
		notaT.notaTotal = 0
		notaT.save()
		for e in ev.iterator():
			for g in grades.iterator():
				if e.id == g.id_evaluacion.id:

					notaT.notaTotal = notaT.notaTotal + e.ponderacion*g.nota
					notaT.save()
	except ObjectDoesNotExist:
		alumno = Alumno.objects.get(id = id_subject)
		materia = Materia.objects.get(id = id_ev)
		notaT = NotaGlobal.objects.create(id_alumno = alumno, id_materia = materia, notaTotal = 0.00)
	return render(request, 'consultar_nota.html', {'grades': grades, 'eva':ev, 'notaT' : notaT})

# =============== CRUD Evaluaciones (Docente) ===============
# Objetivo: Gestionar Evaluaciones por Grado
def crear_evaluacion(request):
	if request.method == 'POST':
		form = EvaluacionForm(request.POST)
		if form.is_valid():

			form.save()
		return redirect('elegir_sub')
	else:
		form = EvaluacionForm()
	return render(request, 'create_evaluation.html', {'form': form})

def listar_evaluacion(request, id_sub):
	evaluaciones = Evaluacion.objects.filter(id_materia = id_sub).order_by('id')
	return render(request, 'list_evaluation.html', {'evaluacion': evaluaciones})

def editar_evaluacion(request, id_evaluacion):
	try:
		evaluacion = Evaluacion.objects.get(id = id_evaluacion)
		if request.method == 'GET':
			form = EvaluacionForm(instance = evaluacion)
		else:
			form = EvaluacionForm(request.POST, instance = evaluacion)
			if form.is_valid():
				form.save()
			return redirect('elegir_sub')
		return render(request, 'create_evaluation.html', {'form': form})
	except ObjectDoesNotExist:
		
		return render(request, 'list_evaluation.html', {'evaluacion': ''})
	
def eliminar_evaluacion(request, id_evaluacion):
	try:
		evaluacion = Evaluacion.objects.get(id=id_evaluacion)
		if request.method == 'POST':
			evaluacion.delete()
			return redirect('elegir_sub')

		return render(request, 'del_evaluation.html', {'eval': evaluacion})
	except ObjectDoesNotExist:
		return render(request, 'elegir_materia.html')

#---------------------Vista para el docente--------------------------------------
#vista para consultar los grados y poder dar click en uno para consultar alumnos
#tiene el profesor en el grado que seleccione(puede tener varios) 
@login_required #decorador
# la vista< list_grades_per_level y la vista list_students se relacionan
#la idea es que la primera muestre los grados que tiene el profesor
#y que estos en la template tengan un enlace a la otra vista (list_students)
#para que seleccione al alumno y de ahi ve sus notas en las materias que da
#nota: asi pense la vista no se si les parece, lo puse asi para que tuviera
#un orden porque en una sola tabla creo q se veria algo feo
def list_grades_per_level(request):
		#pido el id del usuario que inició sesión
		encargado = request.user.id
		#pido el docente que tiene ese usuario por medio del id obtenido 
		docente = Docente.objects.get(id=encargado)
		#preguntar si es docente el usuario logeado
		if request.user.groups.filter(name = "Docentes"):
			#obtener los grados del docente que inicio sesion
			grados = Grado.objects.filter(id_encargado=docente.id)
			#renderizar vista para el docente con los grados obtenidos 
		return render(request, 'list_levels.html', {'grado':grados})

def list_students(request, id_grado):
	#en la template se envia el id del grado que uso aqui para filtrar
	#las inscripciones en ese grado y enviarlas al template
	inscripciones = Inscripcion.objects.filter(id_grado=id_grado)
	return render(request, 'list_students.html', {'inscripcion': inscripciones})

#vista para mostrar las materias que el profesor logeado le imparte al alumno que esta
#en el grado en el cual el profesor imparte tal materia
def list_subjects_per_student(request, id_al):
	docente = Docente.objects.get( id=request.user.id)
	materias = docente.materias.all()

	return render(request, 'list_per_student.html', {'materias': materias, 'student':id_al})

def list_subjects_evaluation(request):
	docente = Docente.objects.get(id = request.user.id)
	materias = docente.materias.all()
	return render(request, 'elegir_materia.html',{'materias':materias})



#============= CRUD DE AVISOS (Personal Administrativo, Docentes) =================
#Se devuelve la lista de avisos existentes (podria filtrarse por avisos creados por un usuario para evitar
#que alguien que no es el autor edite un aviso por error. request.user.id == autor_id)
def listar_avisos(request):
	avisos = Aviso.objects.filter(autor = request.user.id).order_by('id')
	return render(request, 'avisos/listar_avisos.html', {'avisos': avisos})

def crear_avisos(request):
	titulo = "Creando Aviso" #Para encabezado de la pagina, pues carga el mismo template que editar.
	if request.method == 'POST': 
		form = AvisoForm(request.POST)
		if form.is_valid():
			objeto = form.save(commit = False) #Se guarda el formulario en un objeto pero no en la base.
			objeto.autor_id = request.user.id #objeto.autor_id agrega un campo al formulario a nivel de codigo para guardar el autor.
			objeto.save() #El objeto con todo y su autor se guarda.
			return redirect(listar_avisos) #Despues de crear un objeto, regresa a la lista de avisos.
	else:
		form = AvisoForm() #Si no se ha presionado guardar, carga vacio el formulario.
	return render(request, 'avisos/crear_editar_avisos.html', {'form': form, 'titulo': titulo})

#Recibe por url el ID de la instancia de aviso que quiere eliminarse.
def eliminar_aviso(request, id_aviso):
	try:
		aviso = Aviso.objects.get(id=id_aviso) #Se busca en la base la instancia que se quiere eliminar.
		if request.method == 'POST':
			aviso.delete() 
			return redirect(listar_avisos) #Despues de eliminar, regresa a la lista de avisos
		return render(request, 'avisos/eliminar_avisos.html', {'aviso': aviso})

	except ObjectDoesNotExist:
		return redirect(lista_avisos)

#Recibe por url, el ID del aviso que quiere modificarse.
def editar_avisos(request, id_aviso):
	titulo = "Editando Aviso" #Para colocar correcto el titulo del template.
	try:
		aviso = Aviso.objects.get(id = id_aviso) #Se carga de la base la instancia de aviso.
		if request.method == 'GET':
			form = AvisoForm(instance = aviso) #Se rellena el formulario con la data del aviso cargado.
		else:
			form = AvisoForm(request.POST, instance = aviso) #Se sobreescribe la instancia de aviso al guardar.
			if form.is_valid():
				form.save()
			return redirect(listar_avisos) #Vuelve a la lista de avisos despues de actualizar.
		return render(request, 'avisos/crear_editar_avisos.html', {'form': form, 'titulo':titulo})

	except ObjectDoesNotExist:
		return redirect(listar_avisos)

#avisos para alumnos
def mostrar_avisos(request):
	alumno = Alumno.objects.get(id_usuario = request.user.id)
	avisos = Aviso.objects.filter(destinatario__isnull= True)
	avisosPrivados = Aviso.objects.filter(destinatario=alumno.id)
	return render(request, 'avisos/avisos_por_alumno.html', {'avisos': avisos, 'privados': avisosPrivados})




#============== CRUD MATERIAS (Personal Administrativo) =================
# Objetivo: Almacenar materias a traves del personal administrativo
# Autores: Edwin Dimas, Kendal Sosa

def crear_materia(request):
	titulo = "Creando Materia"
	if request.method == 'POST':
		form = MateriaForm(request.POST)
		if form.is_valid():
			form.save()
			return redirect(listar_materia)
	else:
		form = MateriaForm
	return render(request, 'materias/crear_editar_materias.html', {'form':form,'titulo':titulo})

def editar_materia(request, id_materia):
	titulo = "Editando Materia"
	try:
		materia = Materia.objects.get(id = id_materia)
		if request.method == 'GET':
			form = MateriaForm(instance = materia)
		else:
			form = MateriaForm(request.POST, instance = materia)
			if form.is_valid():
				form.save()
			return redirect(listar_materia)
		return render(request, 'materias/crear_editar_materias.html', {'form': form, 'titulo': titulo})

	except ObjectDoesNotExist:
		return redirect(listar_materia)

def eliminar_materia(request, id_materia):
	try:
		materia = Materia.objects.get(id = id_materia)
		if request.method == 'POST':
			materia.delete()
			return redirect(listar_materia)
		return render(request, 'materias/eliminar_materias.html', {'materia': materia})
	except ObjectDoesNotExist:
		return redirect(listar_materia)

def listar_materia(request):
	materias = Materia.objects.all().order_by('id')
	return render(request, 'materias/listar_materias.html', {'materias': materias})

#==============CRUD ESCUELA (Personal Administrativo) =================
# Objetivo: Gestionar EScuelas desde la Interfaz del Personal Administrativo

def crear_escuela(request):
	titulo = "Creando Escuela" 
	if request.method == 'POST': 
		form = EscuelaForm(request.POST)
		if form.is_valid():
			form.save() 
			return redirect(listar_escuela) 
	else:
		form = EscuelaForm() 
	return render(request, 'escuela/crear_editar_escuela.html', {'form': form, 'titulo': titulo})

def editar_escuela(request, id_escuela):
	titulo = "Editando escuela"
	try:
		escuela = Escuela.objects.get(id = id_escuela) 
		if request.method == 'GET':
			form = EscuelaForm(instance = escuela) 
		else:
			form = EscuelaForm(request.POST, instance = escuela)
			if form.is_valid():
				form.save()
			return redirect(listar_escuela) 
		return render(request, 'escuela/crear_editar_escuela.html', {'form': form, 'titulo':titulo})

	except ObjectDoesNotExist:
		return redirect(listar_escuela)

def eliminar_escuela(request, id_escuela):
	try:
		escuela = Escuela.objects.get(id=id_escuela) #Se busca en la base la instancia que se quiere eliminar.
		if request.method == 'POST':
			escuela.delete()
			return redirect(listar_escuela) #Despues de eliminar, regresa a la lista de avisos
		return render(request, 'escuela/eliminar_escuela.html', {'escuela': escuela})

	except ObjectDoesNotExist:
		return redirect(listar_escuela)


def listar_escuela(request):
	escuelas = Escuela.objects.all().order_by('id')
	return render(request, 'escuela/listar_escuela.html', {'escuelas': escuelas})

# ========== CRUD Inscripciones (Personal Administrativo) ==========
# Objetivo: Realizar inscripciones para los alumnos en un grado en especifico
# Autor: Kendal Sosa (kendalalfonso37)
def listar_inscripciones(request):
	inscripciones = Inscripcion.objects.all().order_by('fechaHoraInscripcion')
	return render(request, 'inscripcion/listar_inscripcion.html', {'inscripciones': inscripciones})

def crear_inscripcion(request):
	if request.method == 'POST':
		form = InscripcionForm(request.POST)
		if form.is_valid():
			form.save()
			return redirect(listar_inscripciones)
	else:
		form = InscripcionForm()
		return render(request, 'inscripcion/crear_inscripcion.html', {'form': form})

def editar_inscripcion(request, id_inscripcion):
	try:
		inscripcion = Inscripcion.objects.get(id=id_inscripcion)
		if request.method == 'GET':
			form = InscripcionForm(instance = inscripcion)
		else:
			form = InscripcionForm(request.POST, instance = inscripcion)
			if form.is_valid():
				form.save()
				return redirect(listar_inscripciones)
		return render(request, 'inscripcion/editar_inscripcion.html', {'form': form})
	except ObjectDoesNotExist:
		return redirect(listar_inscripciones)

def eliminar_inscripcion(request, id_inscripcion):
	try:
		inscripcion = Inscripcion.objects.get(id=id_inscripcion)
		if request.method == 'POST':
			inscripcion.delete()
			return redirect(listar_inscripciones)
		return render(request, 'inscripcion/eliminar_inscripcion.html', {'inscripcion':inscripcion})
	except ObjectDoesNotExist:
		return redirect(listar_inscripciones)

#===============CRUD Alumnos==================
#Pense en crear a los alumnos de una forma un poco mas dinamica. Un formulario dinamico trae del template 
#una cantidad cualquiera de alumnos con nombre y apellido. A partir de ahi, se genera un carnet con el mismo
#algoritmo de la ues. Se agrega el usuario, se le genera una password random, se agrega y asocia en la tabla 
#Alumno. Se usan los datos del admin, para asociarle la escuela donde estudia. 
#Se devuelve en un PDF al administrador, el listado de alumnos con nombre, usuario y clave de acceso.
#Para que puedan entregarla a los alumnos. Ojo que la clave no se guarda nunca, se crea el PDF mientras se
#crea el usuario, se descarga y se pierde.
#Autor: Edwin Dimas 
def crear_alumno(request):
	#Variables que contendran los datos del formulario.
	nombres = []
	apellidos = []
	# r contiene las iniciales 
	r = []
	carnet = ""
	anio = datetime.datetime.now().year
	anio = anio - 2000
	#Anio termina con el valor del anio en dos digitos ej: 17
	#correlativo debe contener el valor del numero correlativo del carnet ej: 002, 003 
	correlativo = ""
	#Aux sirve para iterar en la consulta sin alterar el correlativo.
	aux = 0
	#Se va a autogenerar una password para cada alumno.
	password = "" 
	#numero Devuelve la queryset de la consulta raw. 
	numero = {}
	#arreglo guarda cada linea de la tabla para el pdf
	arreglo = []
	#Se genera una instancia del usuario administrador que ha iniciado sesion.
	escuelaAdmin = PersonalAdministrativo.objects.get(id_usuario = request.user.id)
	grupoAlumnos = Group.objects.get(name = 'Alumnos')
	if request.method == 'POST':
		#Se devuelve la info del form.
		nombres = request.POST.getlist('Nombre')
		apellidos = request.POST.getlist('Apellido')
		#Se itera con ambas variables a la vez.
		for i, j in zip(nombres, apellidos):
			#Se busca la inicial de cada apellido y se guarda en r.
			for h in j.upper().split():
				r += h[0]
			#Se agrega el contenido de r al carnet (las iniciales)
			for x in r:
				carnet = carnet + x
			#Se agrega el anio al carnet (iniciales + anio)
			carnet = carnet + str(anio)   
			#se revisa en la base los que tienen u carnet que comienza igual para sacar el correlativo.                                             
			numero = list(User.objects.raw("Select * from auth_user where username like "+ "'"+carnet+"%%'" ))
			#Como django no le gustan mucho las cosas como count(id). Sino que siempre devuelve una queryset
			#se itera el resultado para contar a pie el correlativo.
			for x in numero:
				aux = aux + 1
			#se agrega al correlativo 1001 y luego se trunca el digito mas significativo.
			#si aux = 2 (ya existen dos carnets). correlativo = 1003, se trunca el 1 queda 003 
			correlativo = str(aux + 1001)
			correlativo = correlativo[1:]
			#Carnet final (Iniciales + anio + correlativo)
			carnet = carnet + correlativo
			password = User.objects.make_random_password()
			newuser = User.objects.create_user(carnet, '',  password)
			arreglo.append((i+" "+j, carnet, password))
			#Se crea la funcion create_alumno y se agrega a objects. Ver en models.py
			alumno = Alumno.objects.create_alumno(i+" "+j, escuelaAdmin.id_escuela ,newuser)
			#Se agregan al grupo de alumnos para que accesen al index correspondiente.
			newuser.groups.add(grupoAlumnos)
			carnet = ""
			r = []
			aux = 0
			password = ""
		#Se descarga el pdf.
		resp = generarPDF(arreglo)

		return resp

	return render(request, 'alumnos/alumnos_crear.html', {})

#Procedimiento bastante comun entre nosotros en editar, eliminar y listar alumnos.
#En este caso editar no se hace junto con crear, la diferencia de la logica es demasiado grande
#como para juntarlos.
def eliminar_alumno(request, user_id):
	try:
		alumno = Alumno.objects.get(id=user_id)
		if request.method == 'POST':
			alumno.delete()
			return redirect(listar_alumno)
		return render(request, 'alumnos/alumnos_eliminar.html', {'alumno':alumno})
	except ObjectDoesNotExist:
		return redirect(listar_alumno)
 
def editar_alumno(request, user_id):
	try:
		alumno = Alumno.objects.get(id=user_id)
		if request.method == 'GET':
			form = AlumnoForm(instance = alumno)
		else:
			form = AlumnoForm(request.POST, instance = alumno)
			if form.is_valid():
				form.save()
				return redirect(listar_alumno)
		return render(request, 'alumnos/alumnos_editar.html', {'form': form})
	except ObjectDoesNotExist:
		return redirect(listar_alumno)

def listar_alumno(request):
	alumnos = Alumno.objects.all().order_by('id')
	return render(request, 'alumnos/alumnos_listar.html', {'alumnos' : alumnos})

#===============CRUD Grados===================

def crear_grado(request):
	titulo = "Creando un Grado nuevo en su Escuela"
	administrativo = PersonalAdministrativo.objects.get(id_usuario=request.user.id)
	if request.method == 'POST':
		form = GradoForm(request.POST)
		if form.is_valid():
			formulario = form.save(commit=False)
			formulario.id_escuela = administrativo.id_escuela
			formulario.save()
			return redirect(listar_grado)
	else:
		form = GradoForm()
		return render(request, 'grados/grados_crear_editar.html', {'form': form, 'titulo': titulo})

def eliminar_grado(request, id_grado):
	try:
		grado = Grado.objects.get(id=id_grado)
		if request.method == 'POST':
			grado.delete()
			return redirect(listar_grado)
		return render(request, 'grados/grados_eliminar.html', {'grado':grado})
	except ObjectDoesNotExist:
		return redirect(listar_grado)

def editar_grado(request, id_grado):
	titulo = ""
	try:
		grado = Grado.objects.get(id = id_grado)
		titulo = "Editando grado: "+ grado.nombreGrado
		if request.method == 'GET':
			form = GradoForm(instance = grado)
		else:
			form = GradoForm(request.POST, instance = grado)
			if form.is_valid():
				form.save()
				return redirect(listar_grado)
		return render(request, 'grados/grados_crear_editar.html', {'form': form, 'titulo': titulo})
	except ObjectDoesNotExist:
		return redirect(listar_grado)

def listar_grado(request):
	grados = Grado.objects.all().order_by('id')
	return render(request, 'grados/grados_listar.html', {'grados' : grados})


#=======Fin CRUD Grados========

#Esta funcion genera el PDF para el listado de alumnos.
#Retorna: Una HttpResponse. Conteniendo el pdf. Por eso al retornar en una vista cualquiera, 
#se puede poner sin problema "return generarPDF(datos)"
#Para generar otro tipo de documento, hay que cambiar los textos quemados en este codigo. como header y footer
#Autor: Edwin Dimas
def generarPDF(arreglo):
	#Se instancia el valor de retorno
	response = HttpResponse(content_type='application/pdf')
	#Le asigne un nombre con fecha y hora, para que siempre sea unico y facil identificar.
	pdf_name = "SIRAIPS_" + str(datetime.datetime.now())[:19] + "_Reg_Estudiantes.pdf"
	response['Content-Disposition'] = 'attachment; filename=%s' % pdf_name
	#Este es un buffer temporal para el contenido.
	buff = BytesIO()
	#configuracion basica del documento
	doc = SimpleDocTemplate(buff,
	                        pagesize=letter,
	                        rightMargin=40,
	                        leftMargin=40,
	                        topMargin=60,
	                        bottomMargin=18,
	                        )
	#Este arreglo termina recibiendo la data en raw.
	estudiantes = []
	#Estilos para los textos.
	styles = getSampleStyleSheet()
	#Se genera el texto que nunca cambia en todos los archivos. (header, subtitulo, footer, headings)
	#aparecen en el archivo en el orden en que se agreguen ( .append(elemento) )
	header = Paragraph("SIRAIPS -- Registro de Estudiantes", styles['Heading1'])
	subtitulo = Paragraph("Usuarios nuevos creados en: "+ str(datetime.datetime.now())[:19], styles['Heading3'])
	footer = Paragraph("**No debe perder este documento, las claves de acceso solo son entregadas una vez. Por seguridad se guardan en SIRAIPS unicamente cifradas.", styles['Heading4'])
	estudiantes.append(header)
	estudiantes.append(subtitulo)
	headings = ('Nombre', 'Usuario', 'Clave de Acceso')

	t = Table([headings] + arreglo)
	#Aqui se le da estilo a la tabla. Me gusto el que venia por defecto, asi que ya no investigue como 
	#hacer cambios a esta parte. Queda a criterio de quien lo use.
	t.setStyle(TableStyle(
	    [
	        ('GRID', (0, 0), (3, -1), 1, colors.dodgerblue),
	        ('LINEBELOW', (0, 0), (-1, 0), 2, colors.darkblue),
	        ('BACKGROUND', (0, 0), (-1, 0), colors.dodgerblue)
	    ]
	))
	estudiantes.append(t)
	estudiantes.append(footer)
	#Se construye el doc con el contenido del arreglo.
	doc.build(estudiantes)
	#Se escribe el buffer en la response.
	response.write(buff.getvalue())
	buff.close()
	return response

# =============== CRUD: Periodos (DOcentes) ===============
# Objetivo: Permite al Docente tener un mayor control sobre
# Los periodos en los que esta trabajando el mismo
# Autor: Kendal Sosa (kendalalfonso37)

def listar_periodos(request):
	periodos = Periodo.objects.all().order_by('id_grado')
	return render(request, 'periodo/listar_periodos.html', {'periodos': periodos})

def crear_periodo(request):
	if request.method == 'POST':
		form = PeriodoForm(request.POST)
		if form.is_valid():
			form.save()
			return redirect(listar_periodos)
	else:
		form = PeriodoForm()
		return render(request, 'periodo/crear_periodo.html', {'form': form})

def editar_periodo(request, id_periodo):
	try:
		periodo = Periodo.objects.get(id=id_periodo)
		if request.method == 'GET':
			form = PeriodoForm(instance = periodo)
		else:
			form = PeriodoForm(request.POST, instance = periodo)
			if form.is_valid():
				form.save()
				return redirect(listar_periodos)
		return render(request, 'periodo/editar_periodo.html', {'form': form})
	except ObjectDoesNotExist:
		return redirect(listar_periodos)

def eliminar_periodo(request, id_periodo):
	try:
		periodo = Periodo.objects.get(id=id_periodo)
		if request.method == 'POST':
			periodo.delete()
			return redirect(listar_periodos)
		return render(request, 'periodo/eliminar_periodo.html', {'periodo':periodo})
	except ObjectDoesNotExist:
		return redirect(listar_periodos)
