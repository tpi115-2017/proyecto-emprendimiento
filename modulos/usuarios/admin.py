# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import User

# from modulos.usuarios.models import Usuario, Rol

# Register your models here.
# admin.site.register(Rol)

# Definiendo un descriptor del modelo Usuario 
#class UsuarioInline(admin.StackedInline):
#	model = Usuario
#	can_delete = False
#	verbose_name_plural = 'Usuarios'

# Definiendo el nuevo modelo User para el admin
#class UserAdmin(BaseUserAdmin):
#	inlines = (UsuarioInline, )

# Registrando nuevamente al UserAdmin; Por el momento se registraran nuevos Usuarios con el Admin de Django
#admin.site.unregister(Usuario)
#BaseUserAdmin como argumento para guardar los usuarios 
# admin.site.register(Usuario)