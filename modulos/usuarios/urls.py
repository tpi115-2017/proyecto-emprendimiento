from django.conf.urls import url, include
from django.urls import reverse_lazy
#from modulos.usuarios import views
from django.contrib.auth.views import LoginView
from django.views.generic import RedirectView


urlpatterns = [	
	url(r'^login/$', LoginView.as_view(template_name='usuarios/login.html'), name='login'),

	# url(r'^profile/$', RedirectView.as_view(url='/siraips/')), # Esta URL redirige a Siraips ahora xD F.Kendal

]